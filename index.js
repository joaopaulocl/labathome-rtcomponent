var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var cors = require('cors');
var bodyParser = require('body-parser');
var Sequelize = require('sequelize');
var sequelize = new Sequelize('mysql://root:rexlabgateway@localhost/labathome');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cors());

var models = require('./models.js');
var Collabs = models.collabs;
var Users = models.users;
var CollabsUsers = models.collabs_users;
var Messages = models.messages;
var Files = models.files;
var Tasks = models.tasks;

io.on('connection', function (socket) {
    var room = socket.handshake['query']['r_var'];
    socket.token = socket.handshake['query']['token'];

    if (room && socket.token) {

        Users.findOne({
            attributes: ['email', 'name', 'id', 'avatar', 'role', 'status'],
            where: {session_token: socket.token},
            raw: true
        }).then(function (user) {
            if (user) {
                Collabs.findOne({
                    where: {hash: room},
                    raw: true
                }).then(function (collab) {
                    if (collab) {
                        user.status = 'active';
                        addUserColabRoom(socket, collab, user);
                    } else {
                        console.log('Colab ' + room + ' nao existe');
                        socket.emit('erro', 'Cannot find ' + room);
                    }
                });

            } else {
                console.log('Usuário ' + socket.username + ' nao existe');
                socket.emit('erro', 'Cannot find ' + socket.username);
            }

        });

    } else {
        socket.emit('erro', 'Missing query parameters');
    }
});

function checkData(obj, elements) {
    var output = {};
    for (var i = 0; i < elements.length; i++) {
        if (obj.hasOwnProperty(elements[i])) {
            output[elements[i]] = obj[elements[i]];
        }
    }
    return output;
}

function addUserColabRoom(socket, collab, thisUser) {
    socket.join(collab.hash);

    console.log('\nuser joined room #' + collab.hash + '\n');

    socket.broadcast.to(collab.hash).emit('user joined', thisUser);

    Users.update({
        status: 'active'
    }, {
        where: {
            id: thisUser.id
        }
    });

    Users.findAll({
        attributes: ['email', 'name', 'id', 'avatar', 'role', 'status'],
        where: {id: {$ne: thisUser.id}},
        include: [{
                model: CollabsUsers,
                where: {collab_id: collab.id}
            }],
        raw: true
    }).then(function (users) {
        if (users) {
            socket.emit('users', users);
        }
    });


    Messages.findAll({
        limit: 10,
        order: [['created_at', 'DESC']],
        where: {collab_id: collab.id},
        raw: true,
        include: [{
                model: Users,
                attributes: ['email', 'name', 'id', 'avatar', 'role', 'status'],
            }]
    }).then(function (msgs) {
        if (msgs) {
            socket.emit('last messages', {messages: msgs, limit: msgs.length, offset: 0});
        }

    });

    Tasks.findAll({
        order: [['order', 'DESC']],
        where: {collab_id: collab.id},
        raw: true
    }).then(function (tasks) {
        if (tasks) {
            socket.emit('load tasks', tasks);
        }

    });

    Files.findAll({
        order: [['created_at', 'DESC']],
        where: {collab_id: collab.id},
        raw: true
    }).then(function (files) {
        if (files) {
            socket.emit('load files', files);
        }
    });


    socket.on('more messages', function (data) {
        if (isNaN(data.limit) && isNaN(data.offset)) {
            console.log('invalid parameters');
            socket.emit('erro', 'Some parameters are missing. Expecting something like {limit : Number, offset: Number}');
            return;
        }
        Messages.findAll({
            where: {collab_id: collab.id},
            order: [['created_at', 'DESC']],
            limit: data.limit,
            offset: data.offset,
            raw: true, // se usar raw true a saída da junção será user.email, senão será um objeto a parte
            include: [{
                model: Users,
                attributes: ['email', 'name', 'id', 'avatar', 'role', 'status'],
            }]
        }).then(function (msgs) {
            if (msgs) {
                socket.emit('last messages', {messages: msgs, limit: msgs.length, offset: data.offset});
            }
        });
    });

    socket.on('new message', function (data) {
        Messages.create({payload: data, sent_by: thisUser.id, collab_id: collab.id});
        var output = {};
        for (var key in thisUser) {
            if (thisUser.hasOwnProperty(key)) {
                output['user.' + key] = thisUser[key];
            }
        }
        output.payload = data;
        output.created_at = new Date();
        socket.broadcast.to(collab.hash).emit('new message', output);
    });

    socket.on('typing', function () {
        socket.broadcast.to(collab.hash).emit('typing', thisUser);
    });

    socket.on('stop typing', function () {
        socket.broadcast.to(collab.hash).emit('stop typing', thisUser);
    });

    socket.on('create task', function (data_in) {
        var data_store = checkData(data_in, ['due_date', 'description', 'name', 'assigned_to', 'order']);
        data_store.assignee = thisUser.id;
        data_store.collab_id = collab.id;

        if (isNaN(data_store.order)) { // não foi definida a ordem
            Tasks.max('order', {where: {collab_id: collab.id}}).then(function (max) {
                
                if (isNaN(max))
                    data_store.order = 0;
                else
                    data_store.order = max + 1;

                createTask(data_store);
            });
        } else {
            Tasks.findOne({where: {collab_id: collab.id, order: data_store.order}}).then(function (task) {
                if (task) {
                    Tasks.update({
                        order: sequelize.literal('`order`+1')
                    }, {
                        where: {
                            collab_id: collab.id,
                            order: {$gte: data_store.order}
                        }
                    }).then(function () {
                        createTask(data_store);
                    });
                } else {
                    createTask(data_store);
                }
            });

        }

        function createTask(data) {
            Tasks.create(data).then(function (task) {
                if (task) {
                    io.to(collab.hash).emit('new task', task.dataValues);
                } else {
                    socket.emit('erro', {message: 'Error while creating task', output: task});
                }
            });
        }

    });

    socket.on('update task', function (data) {

        if (!isNaN(data.id)) {
            var task_id = data.id;
            var data_store = checkData(data, ['due_date', 'description', 'name', 'assigned_to', 'finished_at', 'order', 'accomplished_by', 'order']);

            if (!isNaN(data.order)) { // não foi definida a ordem

                Tasks.findOne({where: {collab_id: collab.id, order: data_store.order}}).then(function (task) {
                    if (task) {
                        Tasks.update({
                            order: sequelize.literal('`order`+1')
                        }, {
                            where: {
                                collab_id: collab.id,
                                order: {$gte: data_store.order}
                            }
                        }).then(function () {
                            updateTask(data_store);
                        });
                    } else {
                        updateTask(data_store);
                    }

                });

            } else {
                updateTask(data_store);
            }

            function updateTask(data_store) {
                Tasks.update(data_store, {
                    where: {
                        id: task_id,
                        collab_id: collab.id
                    }
                }).then(function (ret) {
                    if (ret[0]) {
                        Tasks.findOne({
                            where: {
                                id: task_id,
                                collab_id: collab.id
                            }, 
                            raw: true
                        }).then(function (taskobj) {
                            io.to(collab.hash).emit('task updated', taskobj);
                        });

                    } else {
                        socket.emit('erro', {message: 'Error while updating task', output: ret});
                    }
                });
            }
        } else {
            socket.emit('erro', 'Parameter id is missing. Expecting something like {id : Number}');
        }

    });

    socket.on('delete task', function (data) {
        if (!isNaN(data.id)) {
            Tasks.destroy({
                where: {
                    id: data.id,
                    collab_id: collab.id
                },
                force: true
            }).then(function (deleted) {
                if (deleted) {
                    io.to(collab.hash).emit('task deleted', data);
                } else {
                    socket.emit('erro', {message: 'Error while deleting task', output: deleted});
                }

            });
        } else {
            socket.emit('erro', 'Parameter id is missing. Expecting something like {id : Number}');
        }
    });

    socket.on('create file', function (data) {
        data = checkData(data, ['name', 'extension', 'path', 'size']);
        data.uploaded_by = thisUser.id;
        data.collab_id = collab.id;
        Files.create(data).then(function (file) {
            if (file) {
                socket.broadcast.to(collab.hash).emit('new file', file.dataValues);
            } else {
                socket.emit('erro', {message: 'Error while creating file', output: file});
            }
        });

    });

    socket.on('update file', function (data) {
        if (!isNaN(data.id)) {
            var file_id = data.id;
            var data_store = checkData(data, ['name', 'extension', 'path', 'size']);
            Files.update(data_store, {
                where: {
                    id: file_id,
                    collab_id: collab.id
                }
            }).then(function (file) {
                if (file[0]) {
                    io.to(collab.hash).emit('file updated', data);
                } else {
                    socket.emit('erro', {message: 'Error while updating task', output: file});
                }
            });
        } else {
            socket.emit('erro', 'Parameter id is missing. Expecting something like {id : Number}');
        }

    });

    socket.on('delete file', function (data) {
        if (!isNaN(data.id)) {
            Files.destroy({
                where: {
                    id: data.id,
                    collab_id: collab.id
                },
                force: true
            }).then(function (deleted) {
                if (deleted) {
                    io.to(collab.hash).emit('file deleted', data);
                } else {
                    socket.emit('erro', {message: 'Error while deleting file', output: deleted});
                }
            });
        } else {
            socket.emit('erro', 'Parameter id is missing. Expecting something like {id : Number}');
        }
    });

    socket.on('disconnect', function () {
        thisUser.status = 'inactive';
        socket.broadcast.to(collab.hash).emit('user left', thisUser);
        Users.update({
            status: 'inactive',
        }, {
            where: {
                id: thisUser.id
            }
        });
        socket.leave(collab.hash)
    });
}


app.use(express.static(__dirname + '/public'));

http.listen('8080', function () {
    console.log('server listening on port 8080');

});
 