var Sequelize = require('sequelize');
var sequelize = new Sequelize('mysql://root:rexlabgateway@localhost/labathome');

var models = {};

models.collabs = {
    id: {
        type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true
    },
    created_by: {
        type: Sequelize.INTEGER
    },
    finished: {
        type: Sequelize.INTEGER
    },
    hash: {
        type: Sequelize.STRING
    },
    name: {
        type: Sequelize.STRING
    }

};

models.collabs_users = {
    id: {
        type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true
    },
    collab_id: {
        type: Sequelize.INTEGER,
        references: {
            model: 'collabs',
            key: 'id'
        }
    },
    user_id: {
        type: Sequelize.INTEGER,
        references: {
            model: 'users',
            key: 'id',
        }
    }
};

models.users = {
    name: {
        type: Sequelize.STRING
    },
    avatar: {
        type: Sequelize.STRING
    },
    id: {
        type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true
    },
    email: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.STRING
    },
    session_token: {
        type: Sequelize.STRING,
    },
    role: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    remember_token: {
        type: Sequelize.STRING
    }
    

};

models.messages = {
    payload: {
        type: Sequelize.TEXT
    },
    id: {
        type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, unique: true, allowNull: true 
    },
    sent_by: {
        type: Sequelize.INTEGER,
        references: {
            model: this.users,
            key: 'id'
        }
    },
    collab_id: {
        type: Sequelize.INTEGER,
        references: {
            model: this.collabs,
            key: 'id'
        }
    }
};

models.files = {
    name: {
        type: Sequelize.STRING
    },
    id: {
        type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true
    },
    path: {
        type: Sequelize.STRING
    },
    extension: {
        type: Sequelize.STRING
    },
    size: {
        type: Sequelize.STRING
    },
    uploaded_by: {
        type: Sequelize.INTEGER,
        references: {
            model: this.users,
            key: 'id'
        }
    },
    collab_id: {
        type: Sequelize.INTEGER,
        references: {
            model: this.collabs,
            key: 'id'
        }
    }
};

models.tasks = {
    name: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    id: {
        type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true
    },
    due_date : {
        type: Sequelize.DATE
    },
    finished_at: {
        type: Sequelize.DATE
    },
    order: {
        type: Sequelize.INTEGER
    },
    assignee: {
        type: Sequelize.INTEGER,
        references: {
            model: this.users,
            key: 'id'
        }
    },
    accomplished_by: {
        type: Sequelize.INTEGER,
        references: {
            model: this.users,
            key: 'id'
        }
    },
    assigned_to: {
        type: Sequelize.INTEGER,
        references: {
            model: this.users,
            key: 'id'
        }
    },
    collab_id: {
        type: Sequelize.INTEGER,
        references: {
            model: this.collabs,
            key: 'id'
        }
    }
};


exports.collabs  = sequelize.define('collabs', models.collabs, {
    freezeTableName: true, underscored: true
});

exports.users  = sequelize.define('users', models.users, {
    freezeTableName: true, underscored: true
});

exports.collabs_users = sequelize.define('collabs_users', models.collabs_users, {
    freezeTableName: true, underscored: true
});

exports.messages = sequelize.define('messages', models.messages, {
    freezeTableName: true, underscored: true
});

exports.files = sequelize.define('files', models.files, {
    freezeTableName: true, underscored: true
});

exports.tasks = sequelize.define('collabs_tasks', models.tasks, {
    freezeTableName: true, underscored: true
});


this.users.belongsToMany(this.collabs, { through: this.collabs_users });
this.collabs.belongsToMany(this.users, { through: this.collabs_users });

//this.collabs.hasMany(this.messages, {foreignKey : 'collab_id'});
//this.users.hasMany(this.messages, {foreignKey : 'sent_by'});
this.messages.belongsTo(this.collabs,  {foreignKey : 'collab_id'});
this.messages.belongsTo(this.users, {foreignKey : 'sent_by'});

this.users.hasMany(this.collabs_users, {foreignKey : 'user_id'});
